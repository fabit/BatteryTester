#include "init.h"

/**
 * Init pins, serial and EEPROM.
 */
void initApp(){
  pinMode(LED_OK,OUTPUT);
  pinMode(LED_NOK,OUTPUT);
  pinMode(LED_ALARM,OUTPUT);
  pinMode(LCD_BRIGHTNESS,OUTPUT);
  pinMode(LCD_CONTRAST,OUTPUT);
  pinMode(VOLTAGE_ENABLE,OUTPUT);
  //EEPROM Configuration
  initConfiguration();
  
  Serial.begin(9600);
}
/**
 * Performs led,buttons,lcd and battery test.
 *  
 */
int initAutoTest(LiquidCrystal lcd){

     // ------------------------------------
    // -------------- LED ------------------
    // -------------------------------------
    digitalWrite(LED_NOK   ,HIGH);
    digitalWrite(LED_OK    ,HIGH);
    digitalWrite(LED_ALARM ,HIGH);
    delay(AUTOTEST_LED_DELAY);
    digitalWrite(LED_NOK   ,LOW);
    digitalWrite(LED_OK    ,LOW);
    digitalWrite(LED_ALARM ,LOW);

    // ------------------------------------
    // -------------- BUTTONS -------------
    // ------------------------------------
    
    // CHECK UP BUTTON
    int buttonState = analogRead(BUTTON_UP);
    if (buttonState ==LOW){
         return AUTOTEST_ERROR_BUTTON_UP;
    }
    // CHECK DOWN BUTTON
    buttonState = analogRead(BUTTON_DOWN);
    if (buttonState ==LOW){
         return AUTOTEST_ERROR_BUTTON_DOWN;
    }

    // CHECK SENSITIVITY BUTTON.
    buttonState = analogRead(BUTTON_SENSITIVITY);
    if (buttonState ==LOW){
         return AUTOTEST_ERROR_BUTTON_SENSITIVITY;
    }
    
    // ------------------------------------
    // --------------LCD TEST-------------- 
    // ------------------------------------
    
    // Testing brightness and contrast
    analogWrite(LCD_CONTRAST,LCD_DEFAULT_CONTRAST);
    analogWrite(LCD_BRIGHTNESS,LCD_DEFAULT_BRIGHTNESS);
    lcd.print(AUTOTEST_MESSAGE);
    lcd.display();
    delay(500);
    // Testing brightness and contrast
    analogWrite(LCD_BRIGHTNESS,0);
    analogWrite(LCD_CONTRAST,0);
    delay(500);
    // reset to default.
    analogWrite(LCD_CONTRAST,LCD_DEFAULT_CONTRAST);
    analogWrite(LCD_BRIGHTNESS,LCD_DEFAULT_BRIGHTNESS);
    // reset LCD.
    lcd.clear();
    
    
    // ------------------------------------
    // --------------BATTERY TEST---------- 
    // ------------------------------------
    
    // disable button. now it should be zero.
    
    initBattery();
    delay(1000);
    // --------------------------------------
    // check voltage.
    // -------------------------------------
    
    float battery      = readBattery(true);
    float sensitivity  = getSensitivity();
    // VOLTAGE_SWITCH_OFF_MAX_VOLTAGE is the voltage for the mosfet partitor.
    if (battery-sensitivity > VOLTAGE_SWITCH_OFF_MAX_VOLTAGE){
      Serial.println("Not zero"+(String)(battery-sensitivity));
      return AUTOTEST_ERROR_VOLTAGE_ZERO;
    }
    // -------------------------------------
    
    // Nothing to test if the enable pin is ok since we don't know what is linked.
    // An option would be to tell the user to plug the battery in another time but this would be uncomfortable for the user.
    // For this reason no testing are performed if the switch for the voltage is enable.  
    //digitalWrite(VOLTAGE_ENABLE,HIGH);  

    
    return AUTOTEST_OK;
}
