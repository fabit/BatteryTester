#include "limit_voltage.h"

// voltage array
float voltage[VOLTAGE_LIMIT_SIZE] = {1.2,1.5,2.5,3,3.6};
// voltage index.
int voltageIndex = VOLTAGE_DEFAULT_INDEX;


/**
 * Get the voltage limit.
 */
float getVoltageLimit(){
  return voltage[voltageIndex];
}
/**
 * Choose the next voltage limit.
 * @return boolean true if it change otherwise false.
 */
bool nextVoltageLimit(){
  if (voltageIndex==VOLTAGE_LIMIT_SIZE-1){  
     return false;
  }
  voltageIndex++;
  return true;
}
/**
 * Choose the previous voltage limit.
 * @return boolean true if it change otherwise false.
 */
bool   prevVoltageLimit(){
  if (voltageIndex==0){  
     return false;
  }
  voltageIndex--;
  return true;
  
}

/**
 * Set the next voltage limit index.
 * @param int new_voltindex 
 */
void setVoltageLimitIndex(int new_voltindex){
  voltageIndex = new_voltindex;
}
/**
 * Retrieve the voltage limit
 */
int  getVoltageLimitIndex(){
  return voltageIndex;
}

