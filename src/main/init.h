#ifndef init_H
#define init_H

#include <Arduino.h>
#include <LiquidCrystal.h>
#include "const.h"
#include "battery.h"
#include "sensitivity.h"
#include "limit_voltage.h"
#include "utils.h"
#include "config.h"

/**
 * Init pins, serial and EEPROM.
 */
void initApp();
/**
 * Performs led,buttons,lcd and battery test.
 *  
 */
int initAutoTest(LiquidCrystal lcd);

#endif
