#ifndef config_H
#define config_H

#include <Arduino.h>
#include "const.h"
#include "limit_voltage.h"
#include "sensitivity.h"
#include "EEPROM.h"

/**
 * Check if it is the first time.
 */
void initConfiguration();
/**
 * Load configuration or save if it is the first time.
 */
void loadConfiguration();

/**
 * Save sensitivity
 */
void saveSensitivity(float value);
/**
 * Save voltage limit.
 */
void saveVoltageLimit(int value);

#endif
