#include "battery.h"

// DEFINE BUFFER FOR VOLTAGE.
float voltageSample[VOLTAGE_SAMPLE_SIZE];
// CLEAR MEASURE 
bool clearMeasure = true ;
/**
 * Abilità la misurazione completa.
 */
void enableClearMeasure(){
  clearMeasure = true;
}
/**
 * Inizializza i pin della batteria.
 */
void initBattery(){
  digitalWrite(VOLTAGE_ENABLE,HIGH);
}
/**
 * Effettua la misurazione
 * @param test è utilizzato esclusivamente per motivi di debug il default è false.
 */
float readBattery(bool test){
   int i;
   float avg;
   float maxV,minV;
   float limitValue;
   // Abilita la lettura dal piedino di arduino.
   if (test == false){
    digitalWrite(VOLTAGE_ENABLE,LOW);
   }
   // Modalità completa o parziale.
   if (clearMeasure == true){// completa
    // leggo tutti i sample.
     for (i = 0;i<VOLTAGE_SAMPLE_SIZE;i++){
        voltageSample[i] = analogRead(VOLTAGE_READ);
        delay(VOLTAGE_SAMPLE_RATE); // delay tra i sample.
     }
     clearMeasure = false;// Disabilito la lettura completa.
   }else{
      // effettuo uno shift tra i sample per rimuovere l' elemento più vecchio.
      for (i = 0;i<VOLTAGE_SAMPLE_SIZE-1;i++){
        voltageSample[VOLTAGE_SAMPLE_SIZE-i-1] =  voltageSample[VOLTAGE_SAMPLE_SIZE-i-2];
      }
      voltageSample[0] = analogRead(VOLTAGE_READ); // leggo il nuovo elemento di tensione e lo inserisco dentro l' array.

   }
   // disabilito la lettura del piedino
   if (test == false){
   digitalWrite(VOLTAGE_ENABLE,HIGH);
   }
  // svolgo la media tra tutti i campioni.
   long sum= 0;
   for (i = 0;i<VOLTAGE_SAMPLE_SIZE;i++){
     // SUM VOLTAGE SAMPLE..
     sum+=voltageSample[i];
   }
   avg=sum/(float)VOLTAGE_SAMPLE_SIZE;
   // ottengo il valore in volt.
   avg = avg*0.00488758553275;
   return avg; // restituisco la media.
}


