#include "init.h"
#include "const.h"
#include <LiquidCrystal.h>
#include "buttons.h"
#include "battery.h"
#include "utils.h"
#include "limit_voltage.h"
#include "led_status.h"
#include "power_saving.h"
#include "config.h"

LiquidCrystal lcd(LCD_REGISTER, LCD_ENABLE, LCD_DATA1, LCD_DATA2, LCD_DATA3, LCD_DATA4);

void setup() 
{
      String resultString  = "";
      
      lcd.begin(16, 2);
      initApp();
      
      // AUTOTEST CHECK.
      int result = initAutoTest(lcd);
      // GET THE OUTPUT FROM THE RESULT.
      if (result==AUTOTEST_OK){
        resultString = AUTOTEST_OK_STRING;
      }
      if (result==AUTOTEST_ERROR_BUTTON_UP){
        resultString = AUTOTEST_ERROR_BUTTON_UP_STRING;
      }
      if (result== AUTOTEST_ERROR_BUTTON_DOWN){
        resultString = AUTOTEST_ERROR_BUTTON_DOWN_STRING;
      }
      if (result== AUTOTEST_ERROR_BUTTON_SENSITIVITY){
        resultString = AUTOTEST_ERROR_BUTTON_SENSITIVITY_STRING;
      }
      if (result == AUTOTEST_ERROR_VOLTAGE_ZERO){
        resultString = AUTOTEST_ERROR_VOLTAGE_ZERO_STRING;//readBattery(true);
      }
      // SHOW IT
      // FRONT END.
      // PRINT APP NAME
      lcd.setCursor(0,0);
      lcd.print(APP_NAME);
      Serial.println(APP_NAME);
      lcd.setCursor(0,1);

      // PRINT AUTO TEST STATE.
      Serial.println(resultString);
      lcd.print(resultString);
      lcd.display();

      // IF THERE IS ANY ERROR.
      
      if (result!=AUTOTEST_OK){
          // SET LED TO ERROR.
          setStatus(LED_ERROR);
          // WAIT FOR SHOWING THE OUTPUT.
          delay(1000);
          // EXIT 
          exit(0);
      }
      // LOAD CONFIGURATION
      loadConfiguration();
      
      // SUBSCRIBE BUTTONS.
      subscribeButtonUp(OnButtonUpReleased);
      subscribeButtonDown(OnButtonDownReleased);
      subscribeButtonSensitivity(OnButtonSensitivityReleased);
      
      // INITIAL CLEAR MEASURE.
      performClearMeasure();

      
}

void loop() 
{
   
    
    // PERFORM THE PARTIAL MEASURE
    performPartialMeasure();
    //performClearMeasure();
    // AVOID LOCK STATE.
    // PUBLISH THE BUTTON STATE AND CALL EVENT IF NEEDED.
    for (int i = 0;i<PARTIAL_MEASURE_RATE/10;i++){
      publishButtonUpState(digitalRead(BUTTON_UP));
      publishButtonDownState(digitalRead(BUTTON_DOWN));
      publishButtonSensitivityState(digitalRead(BUTTON_SENSITIVITY));
     // WAIT A LITTLE BIT.
      delay(10);
       
    }
    
    pleaseSleep(); // POWER SAVING.
}
/**
 * called when the button up is released
 */
void OnButtonUpReleased(){
  // GO DOWN WITH THE SCALE . IF IT REALLY GOES DOWN.. THEN CLEAR MEASURE
  if (prevVoltageLimit()==true){
    performClearMeasure();
    saveVoltageLimit(getVoltageLimitIndex());
  }
  // DISABLE POWER SAVING
  wakeUp();
}


/**
 * called when the button down is released
 */
void OnButtonDownReleased(){
  // GO UP WITH THE SCALE . IF IT REALLY GOES UP.. THEN CLEAR MEASURE
  if (nextVoltageLimit()==true){
    performClearMeasure(); 
    saveVoltageLimit(getVoltageLimitIndex());
  }
  // DISABLE POWER SAVING
  wakeUp();
}


/**
 * called when the button sensitivity is released
 */
void OnButtonSensitivityReleased(){
  // CHANGE SENSITIVITY 
  changeSensitivity();
  // SAVE IT 
  saveSensitivity(getSensitivity());
  // DISABLE POWER SAVING
  wakeUp();
}
/**
 * Perform partial measure.
 */
void performPartialMeasure(){
  // PERFORM SIMPLY A CHECK BATTERY
  checkBattery();
}
/**
 * Perform a clear measure
 */
void performClearMeasure(){
  // ENABLE A CLEAR MEASURE
  enableClearMeasure();
  // CHECK BATTERY.
  checkBattery();
}


void checkBattery(){
  // GET VOLTAGE VOLTAGE LIMIT AND SENSITIVITY.
  float voltage = readBattery(false);
  float voltageLimit = getVoltageLimit();
  float sensitivity  = getSensitivity();
  
  // STRIP LAST CHARACTER.FRONT END STUFF
  String voltageString = String(voltageLimit);
  voltageString.remove(voltageString.length()-1);

  // PRINT LCD.
  printMessage(VOLTAGE_CURRENT_STRING+String(voltage)+VOLTAGE_SYMBOL+VOLTAGE_LIMIT_STRING+voltageString+VOLTAGE_SYMBOL+SENSITIVITY_STRING+String(getSensitivity()));

  // POWER SAVING.
  listenEnviroment(voltage);
  
  // CHECK FOR STATUS.
  if (voltage-sensitivity < VOLTAGE_EPSILON){
    setStatus(LED_ALARM);
  }else{ // IF IS NOT ZERO
    // CHECK THE VOLTAGE LIMIT.
    if (checkRange(voltage,voltageLimit,sensitivity)==true){
        setStatus(LED_OK);  // OK 
        
    }else{ // IT IS NOT IN THE RANGE.
      if (getStatus()==LED_ALARM){ // IF BEFORE HAS A ZERO VOLTAGE.
        setStatus(LED_NOK); // PRINT NOT OK AND PERFORM A CLEAR MEASURE
        performClearMeasure();
      }else{
        setStatus(LED_NOK);// PRINT NOT OK.
      }
    }
  }

}
/**
 * Print message to the lcd and serial.
 */
void printMessage(String s){
    lcd.setCursor(0, 1);
    lcd.print(s);
    Serial.println(s);
}
