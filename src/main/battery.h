#ifndef battery_H
#define battery_H

#include <Arduino.h>
#include "const.h"

/**
 * Effettua la misurazione
 * @param test è utilizzato esclusivamente per motivi di debug il default è false.
 */
float readBattery(bool test);
/**
 * Abilità la misurazione completa.
 */
void enableClearMeasure();
/**
 * Inizializza i pin della batteria.
 */
void initBattery();
#endif
