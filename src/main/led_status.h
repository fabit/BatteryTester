#ifndef status_H
#define status_H
#include "const.h"
#include "power_saving.h"
#include <Arduino.h>
/*
 * Set the led status.
 * @param int led the status
 */
void setStatus(int led);
/**
 * Retrieve the status
 */
int getStatus();
#endif
