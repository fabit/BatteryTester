#ifndef buttons_H
#define buttons_H

#include <Arduino.h>
#include "const.h"



/**
 * Sottoscrizione dell' evento del pulsante up.
 * @param onButtonRelease event: l' evento che viene chiamato quando il pulsante viene rilasciato.
 */
void  subscribeButtonUp(onButtonRelease event);
/**
 * Sottoscrizione dell' evento del pulsante down.
 * @param onButtonRelease event: l' evento che viene chiamato quando il pulsante viene rilasciato.
 */
void  subscribeButtonDown(onButtonRelease event);
/**
 * Sottoscrizione dell' evento del pulsante sensitivity.
 * @param onButtonRelease event: l' evento che viene chiamato quando il pulsante viene rilasciato.
 */
void  subscribeButtonSensitivity(onButtonRelease event);



/**
 * Pubblicazione dello stato del pulsante up.
 * @param value stato del pulsante
 */
void  publishButtonUpState(int value);
/**
 * Pubblicazione dello stato del pulsante down.
 * @param value stato del pulsante
 */
void  publishButtonDownState(int value);
/**
 * Pubblicazione dello stato del pulsante sensitivity.
 * @param value stato del pulsante
 */
void  publishButtonSensitivityState(int value);



#endif
