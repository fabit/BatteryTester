#include "buttons.h"
// BUTTON EVENTS
onButtonRelease button_up          = NULL;
onButtonRelease button_down        = NULL;
onButtonRelease button_sensitivity = NULL;
// BUTTON STATE
int button_up_state          = HIGH ;
int button_down_state        = HIGH ;
int button_sensitivity_state = HIGH ;
/**
 * Sottoscrizione del pulsante up.
 * @param onButtonRelease event: the event that will be called when the button up will be release.
 */
void  subscribeButtonUp(onButtonRelease event){
  button_up = event;
}
void  subscribeButtonDown(onButtonRelease event){
  button_down = event;
}
void  subscribeButtonSensitivity(onButtonRelease event){
  button_sensitivity = event;
}

void  publishButtonUpState(int value){
  // check if is it time to call the released up event.
  if (button_up_state==LOW && value==HIGH){
    button_up();
  }
  button_up_state = value;
}
void  publishButtonDownState(int value){
  // check if is it time to call the released down event.
  if (button_down_state==LOW && value==HIGH){
    button_down();
  }
  button_down_state = value;
}
void  publishButtonSensitivityState(int value){
  // check if is it time to call the released down event.
  if (button_sensitivity_state==LOW && value==HIGH){
     button_sensitivity();
  }
  button_sensitivity_state = value;
}

